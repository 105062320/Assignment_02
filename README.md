# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |Y/N|
|:----------------------------------------------------------------------------------------------:|:-----:|:---:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |Y|
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |Y|
|         All things in your game should have correct physical properties and behaviors.         |  15%  |Y|
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |Y|
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |Y|
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |Y|
| Appearance (subjective)                                                                        |  10%  |Y|
| Other creative features in your game (describe on README.md)                                   |  10%  |Y|

## Report 遊戲按鍵皆可用滑鼠點選
**網址:**   
gitlab: https://105062320.gitlab.io/Assignment_02   
firebase: https://assignment2-test.firebaseapp.com/   

1. **A complete game process:**   
遊戲一開始是選單，點擊開始之後進入遊戲，死亡之後進入game over與積分榜畫面，可以選擇重玩或是放棄回至選單。   
<img src="explain3.png" width="340px" height="500px"></img>
2. **basic rules of  "小朋友下樓梯":**   
玩家用左右方向鍵控制角色往兩側移動，不斷向下跳到下一落腳點。
3. **Correct physical properties and behaviors:**   
舉例:玩家採到彈跳磚塊進行彈跳時，與上方磚塊仍會collide。
4. **Interesting traps or special mechanisms:**   
共有6種落腳點-普通地面、踩踏一次之後就會消失的地面、跳板、傳送帶（從左向右或從右向左轉動）   、釘板（踏上後扣減生命值）以及黏黏板(拖慢玩家速度)。
5. **Sound effects and UI:**   
跳板、釘板、黏黏板以及其他道具皆有音效。UI(使用者介面):排版皆有設計，遊戲進行下方欄位會顯示玩家名稱、血條、分數(第幾層)、目前模式以及開始鍵。    
<img src="explain1.png" width="340px" height="500px"></img>
6. **Leaderboard:**   
game over時會顯示積分版，要在一開始選單時填入玩家名稱才會將分數傳至firebase。   
<img src="explain2.png" width="340px" height="500px"></img>
7. **Appearance:**   
有使用google font以及bitmap font來美化字體，積分板已及可以亂數選擇的背景顏色(空白鍵)。

## Other creative features 
* **兩種物品:光環與炸彈:**   
                          吃到圓環時會增加玩家分數;   
                          不小心碰到時炸彈，會爆炸造成傷害。
* **兩種模式:正常與困難:**   
                          正常模式時生命值會回復，有一定機率會出現圓環，可以吃他增加分數;   
                          困難模式時生命值不會回復，有一定機率會出現炸彈，碰到時會扣血。
* **改變背景顏色:**   
在選單以及game over時皆可以使用空白鍵來亂數選擇背景顏色。    
<img src="explain4.png" width="340px" height="500px"></img>
* **暫停功能:**   
遊戲時點擊空白鍵可以暫停遊戲，此時下方使用者欄會出現開始按鍵使用Enter或是滑鼠點擊之皆可解除暫停。

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
