var pause;
var playState = {

    preload:function () {
   
       
   },
   
   create:function  () {
       //game.stage.backgroundColor = backgroundColor;
       //game.input.onDown.add(changeColor, this);
       document.getElementById("User").style.display= "none";
       Wood = game.add.sprite(0, 500, 'wood');
       Wood.height=150;
       Wood.width=400;
       healthbar = game.add.sprite(10,550, 'healthbar');
       healthbar.height=20;
       healthbar.width=380;
       
       //

       //backgroundColor=game.stage.backgroundColor;
       keyboard = game.input.keyboard.addKeys({
           'enter': Phaser.Keyboard.ENTER,
           'up': Phaser.Keyboard.UP,
           'down': Phaser.Keyboard.DOWN,
           'left': Phaser.Keyboard.LEFT,
           'right': Phaser.Keyboard.RIGHT,
           'space':Phaser.Keyboard.SPACEBAR
       });
       pause=0;
       this.healthmeter = game.add.sprite(10,550, 'healthmeter');
       this.healthmeter.width=380;
       this.healthmeter.height=20;
       this.healthmeter.animations.add('life7', [0], 8, false);
       this.healthmeter.animations.add('life6', [1], 8, false);
       this.healthmeter.animations.add('life5', [2], 8, false);
       this.healthmeter.animations.add('life4', [3], 8, false);
       this.healthmeter.animations.add('life3', [4], 8, false);
       this.healthmeter.animations.add('life2', [5], 8, false);
       this.healthmeter.animations.add('life1', [6], 8, false);
       createBounders();
       
       createPlayer();
       createTextsBoard();
       start = game.add.sprite(game.width/2, 610, 'start');
       start.height=70;
       start.width=70;
       start.anchor.setTo(0.5, 0.5);
       start.visible=false;
       start.inputEnabled = true;
       start.events.onInputDown.add(listener, this);
       //game.world.bringToTop(start);
       var style = {fill: '#f200d5',font: '30px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9};
       if(hard_mode==1){
        var modeLabel= game.add.text(330, 600, 'Hard', style);
        modeLabel.anchor.setTo(0.5, 0.5);
       }else{
        var modeLabel= game.add.text(330, 600, 'Normal', style);
        modeLabel.anchor.setTo(0.5, 0.5);
       }
       
   },
   
   update:function  () {
    
       //backgroundColor=game.stage.backgroundColor;
       if(player.body.y>470){
        player.visible=false;
       }
       
       player.body.gravity.y = 500;
       if(keyboard.space.isDown){
        if(status != 'gameOver'){
            
            pause=1;
            start.visible=true;
            pauseLabel.visible=false;
        }
       }
       if(keyboard.enter.isDown){
        //this.clickSound = game.add.audio('click');
        //this.clickSound.play();
        pause=0;
        start.visible=false;
        pauseLabel.visible=true;
       }
       
       // bad
       if(status == 'gameOver'){
         game.state.start('gameover');
       }
       
        if(status != 'running') return;
        
    if(pause==0){
        this.physics.arcade.collide(player, bombs, bomb_effect,null,this);
       this.physics.arcade.collide(player, coins, coin_effect,null,this);
       this.physics.arcade.collide(player, platforms, effect);
       this.physics.arcade.collide(player, [leftWall, rightWall]);
       //healthBar();
       checkTouchCeiling(player);
       
       checkGameOver();
       
       updatePlayer();
       updatePlatforms();
       updateTextsBoard();
   
       createPlatforms();
       if(player.life>6){
            this.healthmeter.animations.play('life7');
        }else if(player.life==6){
            this.healthmeter.animations.play('life6');
        }else if(player.life==5){
            this.healthmeter.animations.play('life5');
        }else if(player.life==4){
            this.healthmeter.animations.play('life4');
        }else if(player.life==3){
            this.healthmeter.animations.play('life3');
        }else if(player.life==2){
            this.healthmeter.animations.play('life2');
        }else if(player.life==1){
            this.healthmeter.animations.play('life1');
        }else if(player.life==0){
            this.healthmeter.visible=false;
        }
    }else{
        player.body.gravity.y = 0;
        player.body.velocity.y=0;
        player.body.velocity.x=0;
    }
   },
   
   
   };
function changeColor() {

    var c = Phaser.Color.getRandomColor(50, 255, 255);

    game.stage.backgroundColor = c;

    //game.fd.record(4);

}
function listener() {
    this.clickSound = game.add.audio('click');
    this.clickSound.play();
    start.visible=false;
    pauseLabel.visible=true;
    pause=0;

}
