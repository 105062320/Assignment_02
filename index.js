//var game = new Phaser.Game(400, 400, Phaser.AUTO, '',
 //   { preload: preload, create: create, update: update });
var game = new Phaser.Game(400, 650, Phaser.AUTO, 'canvas');
var backgroundColor=0;
var player;
var keyboard;
var Wood;
var platforms = [];
var coins = [];
var bombs=[];
var explodes=[];
var healthmeter;
var leftWall;
var rightWall;
var ceiling;
var healthbar;
var text1;
var text2;
var text3;
var title;
var distance = 0;
var status = 'running';
var start;
var  Username;
var lastTime = 0;
var scoreLabel;
var Usertext;
var hard_mode=0;
var normal_text;var hard_text;
var pauseLabel;
   //-----------------------------------------------------------------------
game.global = { score: 0 };
// Add all the states
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('gameover', overState);

// Start the 'boot' state
game.state.start('load');

//-------------------------------------------------------
function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

var lastTime = 0;
function createPlatforms () {
    if(game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform () {
    var onecoin,onebomb;
    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 460;
    var rand = Math.random() * 100;
   
    if(rand < 20) {
        if(hard_mode==1){
            onebomb=game.add.sprite(x+30, y-30, 'bomb');
                game.physics.arcade.enable(onebomb);
                onebomb.animations.add('b', [0,1,2,1], 16, true);
                onebomb.play('b');
                onebomb.scale.setTo(0.25,0.25);
                onebomb.body.immovable = true;
                bombs.push(onebomb);
        }
        if(rand<10){
            if(hard_mode==0){
                onecoin=game.add.sprite(x+30, y-30, 'coin');
                game.physics.arcade.enable(onecoin);
                onecoin.animations.add('coinflip', [0,1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15], 16, true);
                onecoin.play('coinflip');
                onecoin.scale.setTo(0.5,0.5);
                onecoin.body.immovable = true;
                coins.push(onecoin);
            }
        }
        platform = game.add.sprite(x, y, 'normal');
        
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        if(rand<70){
            platform = game.add.sprite(x, y, 'sticky');
        }else{
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        }
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    //game.physics.arcade.enable(onecoin);
    
    platforms.push(platform);
    

    //platform.body.checkCollision.down = false;
    //platform.body.checkCollision.left = false;
    //platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(200, 50, 'player');
    game.physics.arcade.enable(player);
    if(hard_mode==1){
        player.body.gravity.y = 300;
    }else{
        player.body.gravity.y = 500;
    }
    //player.body.gravity.y = 500;
    player.visible=true;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 7;
    
    player.unbeatableTime = 0;
    player.touchOn = undefined;

    this.healthmeter = game.add.sprite(10,550, 'healthmeter');
    this.healthmeter.width=380;
    this.healthmeter.height=20;
    this.healthmeter.animations.add('life7', [0], 8, false);
    this.healthmeter.animations.add('life6', [1], 8, false);
    this.healthmeter.animations.add('life5', [2], 8, false);
    this.healthmeter.animations.add('life4', [3], 8, false);
    this.healthmeter.animations.add('life3', [4], 8, false);
    this.healthmeter.animations.add('life2', [5], 8, false);
    this.healthmeter.animations.add('life1', [6], 8, false);
}

function createTextsBoard () {
    var style = {fill: '#ff0000',font: '20px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9};
    Usertext=text1 = game.add.text(10, 510, Username, {font: '25px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9});
    text1 = game.add.text(10, 580, '', style);
    text2 = game.add.text(330, 510, '', style);
    text3 = game.add.text(140, 200, 'Enter- Restart', style);
    text4 = game.add.text(140, 220, 'Up   - Menu', style);
    pauseLabel= game.add.bitmapText(game.width/2,480, 'myfont','Space-Pause',25);
    pauseLabel.anchor.setTo(0.5, 0.5);
    pauseLabel.visible=true;
    text3.visible = false;
    text4.visible = false;
    
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    if(player.life>6){
        this.healthmeter.animations.play('life7');
    }else if(player.life==6){
        this.healthmeter.animations.play('life6');
    }else if(player.life==5){
        this.healthmeter.animations.play('life5');
    }else if(player.life==4){
        this.healthmeter.animations.play('life4');
    }else if(player.life==3){
        this.healthmeter.animations.play('life3');
    }else if(player.life==2){
        this.healthmeter.animations.play('life2');
    }else if(player.life==1){
        this.healthmeter.animations.play('life1');
    }else if(player.life==0){
        this.healthmeter.visible=false;
    }
    setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}

function updatePlatforms () {
    for(var j=0; j<explodes.length; j++){
        var explode=explodes[j];
        if(hard_mode==1){
            explode.body.position.y -= 2;
        }else{
            explode.body.position.y -= 2;
        }
        //platform.body.position.y -= 3;
        if(explode.body.position.y <= -20) {
            explode.destroy();
            explodes.splice(i, 1);
        }
    }
    for(var j=0; j<bombs.length; j++){
        var bomb=bombs[j];
        if(hard_mode==1){
            bomb.body.position.y -= 2;
        }else{
            bomb.body.position.y -= 2;
        }
        //platform.body.position.y -= 3;
        if(bomb.body.position.y <= -20) {
            bomb.destroy();
            bombs.splice(i, 1);
        }
    }
    for(var j=0; j<coins.length; j++){
        var coin=coins[j];
        if(hard_mode==1){
            coin.body.position.y -= 2;
        }else{
            coin.body.position.y -= 2;
        }
        //platform.body.position.y -= 3;
        if(coin.body.position.y <= -20) {
            coin.destroy();
            coins.splice(i, 1);
        }
    }
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        
        if(hard_mode==1){
            platform.body.position.y -= 2;
        }else{
            platform.body.position.y -= 2;
        }
        //platform.body.position.y -= 3;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:' + player.life);
    text2.setText('B ' + distance);
}
function bomb_effect(player, bomb){
    player.life-=3;
    var explode;
    bomb.kill();
    explode=game.add.sprite(bomb.body.x-20, bomb.body.y-60, 'explode');
    game.physics.arcade.enable(explode);
    this.exSound = game.add.audio('explode');
    explode.animations.add('ex', [0,1,2,3,4,5,6,7,8,10,11], 8, false);
    explodes.push(explode);
    this.exSound.play();
    explode.scale.setTo(0.4,0.4);
    //explode.body.immovable = true;
    explode.play('ex');
    //explode.kill();
    
    //
}
function coin_effect(player, coin){
    distance+=5;
    this.coinSound = game.add.audio('coin');
    this.coinSound.play();
    coin.kill();
}
function effect(player, platform) {
    if(platform.key == 'conveyorRight' && player.body.touching.down) {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft'&& player.body.touching.down) {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline'&& player.body.touching.down) {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails'&& player.body.touching.down) {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal'&& player.body.touching.down) {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake'&& player.body.touching.down) {
        fakeEffect(player, platform);
    }
    if(platform.key == 'sticky'&& player.body.touching.down) {
        stickyEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    this.jumpSound = game.add.audio('jump');
    this.jumpSound.play();
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 1;
        this.hitSound = game.add.audio('hit');
        this.hitSound.play();
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life >= 7) {
            player.life = 7;
        }
        if(player.life < 7 && hard_mode==0) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        this.fakeSound = game.add.audio('fake');
        this.fakeSound.play();
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}
function stickyEffect(player, platform){
        this.sSound = game.add.audio('sticky');
    //if(player.touchOn !== platform) {
        this.sSound.play();
            //player.body.velocity.x+=100;
            
        //player.touchOn = platform;
    //}
        if(player.body.velocity.x>0){
           
            //player.body.velocity.x-=100;
            player.body.x -= 3;
        }else if(player.body.velocity.x<0){
            
            //player.body.velocity.x+=100;
            player.body.x += 3;
        }
        //game.camera.flash(0xff0000, 100);
        
    
};
function checkTouchCeiling(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 1;
            this.hitSound = game.add.audio('hit');
            this.hitSound.play();
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver () {
    if(player.life <= 0 || player.body.y > 470) {
        player.life = 0;
        this.dieSound = game.add.audio('die');
        this.dieSound.play();
        gameOver();
    }
}

function gameOver () {
    explodes.forEach(function(s) {s.destroy()});
    bombs.forEach(function(s) {s.destroy()});
    coins.forEach(function(s) {s.destroy()});
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    bombs=[];
    coins = [];
    explodes=[];
    status = 'gameOver';
}

function restart () {

    text3.visible = false;
    text4.visible = false;
    scoreLabel.visible = false;
    status = 'running';
    distance = 0;
    createPlayer();
    //status = 'restart';
    
}
