var overState = {
    preload:function  () {

    },
    create: function() {
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'Q':Phaser.Keyboard.Q,
            'E':Phaser.Keyboard.E,
            'space':Phaser.Keyboard.SPACEBAR
        });
        status = 'gameOver';
        start.visible=false;
        var style = {fill: '#ff0000',font: '30px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9}
        text1 = game.add.text(10, 530, '', style);
        text2 = game.add.text(350, 530, '', style);
        text3 = game.add.text(80, 625, '<<Restart', style);
        text4 = game.add.text(335, 625, 'Menu>>', style);
        text3.visible = true;
        text4.visible = true;
        text3.anchor.setTo(0.5, 0.5);
        text4.anchor.setTo(0.5, 0.5);
        scoreLabel= game.add.bitmapText(game.width/2, 60, 'myfont','score: ' + distance,50);
        var leaderLabel= game.add.bitmapText(game.width/2, 180, 'myfont','Leader Board',35);
        leaderLabel.anchor.setTo(0.5, 0.5);
        /*scoreLabel = game.add.text(game.width/2, 100,
            'score: ' + distance, {font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9});
        */
        scoreLabel.anchor.setTo(0.5, 0.5);
        scoreLabel.visible = true;
        text3.inputEnabled = true;
        text4.inputEnabled = true;
        var lboard = game.add.image(game.width/2,350, 'lboard');
        lboard.anchor.setTo(0.5, 0.5);
        lboard.width=360;
        lboard.height=300;
        var updates = {};
        if(Username!='unknown'){
            var newScoreKey = firebase.database().ref('board').push().key;
            
            updates['/board/'+newScoreKey] ={
                username:Username,
                score:distance
            }
            
        }
        firebase.database().ref().update(updates).then(function(){
            var i=1;
            var leaderboard=firebase.database().ref('board').orderByChild('score').limitToLast(5);
            leaderboard.on('child_added',function(snapshot){
                if(i<=5){
                    var leaderText= game.add.text((game.width/2)-80, 520-48*i,snapshot.val().username, {font: '20px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 7});
                    var scoreText = game.add.text((game.width/2)+50, 520-48*i,snapshot.val().score, {font: '20px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 7});
                    leaderText.anchor.setTo(0.5, 0.5);
                    scoreText.anchor.setTo(0.5, 0.5);
                    //snapshot.val();
                }
                i++;
            });
            
        });
    },
    update:function  () {
        //gameOver();
        
        
        
        
        if(status == 'gameOver' && keyboard.E.isDown) {
            //restart();
                this.clickSound = game.add.audio('click');
                this.clickSound.play();
                hard_mode=0;
                game.state.start('menu');
            }else if(status == 'gameOver' && keyboard.Q.isDown) {
                this.clickSound = game.add.audio('click');
                this.clickSound.play();
                status = 'running';
                //restart();
                distance = 0;
                game.state.start('play');
            }
            if(status == 'gameOver' && pause==0){
                if(keyboard.space.isDown){
                    changeColor();
                }
                //game.input.onDown.add(changeColor, this);
            
        }
        
        text3.events.onInputDown.add(playtext, this);
        text4.events.onInputDown.add(menutext, this);
    },
};
function playtext(item) {
    this.clickSound = game.add.audio('click');
    this.clickSound.play();
    status = 'running';
    distance = 0;
    game.state.start('play');
    

}
function menutext(item) {
    this.clickSound = game.add.audio('click');
    this.clickSound.play();
    game.state.start('menu');
}