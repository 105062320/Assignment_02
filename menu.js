var menuState = {
    create: function() {
        // Add a background image
        //game.add.image(0, 0, 'background');
        // Display the name of the game
        title = game.add.sprite(game.width/2, 80, 'title');
        
        title.height=80;
        title.width=350;
        title.anchor.setTo(0.5, 0.5);
        scoreLabel= game.add.bitmapText(game.width/2, game.height/2, 'myfont','score: ' + distance,50);
        /*scoreLabel = game.add.text(game.width/2, game.height/2,
            'score: ' + distance, {font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9});
        */
        scoreLabel.anchor.setTo(0.5, 0.5);
        scoreLabel.visible = true;

        // Explain how to start the game
        var startLabel= game.add.bitmapText(game.width/2, game.height-80, 'myfont','Start-Enter',50);
        /*var startLabel = game.add.text(game.width/2, game.height-80,
        'Start', { font: '25px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        */
        startLabel.anchor.setTo(0.5, 0.5);
        startLabel.inputEnabled = true;
        
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'space':Phaser.Keyboard.SPACEBAR
        });

        normal_text = game.add.text(game.width/2, game.height-230,
            'Normal', { fill: '#ff0000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        normal_text.anchor.setTo(0.5, 0.5);
        normal_text.inputEnabled = true;
        hard_text = game.add.text(game.width/2, game.height-180,
            'Hard', { font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        hard_text.anchor.setTo(0.5, 0.5);
        hard_text.inputEnabled = true;
        normal_text.events.onInputDown.add(this.normal_c, this);
        hard_text.events.onInputDown.add(this.hard_c, this);
        
        //game.add.plugin(PhaserInput.Plugin);
        document.getElementById("User").style.display= "block";

        //Username.anchor.setTo(0.5, 0.5);
        
        if(backgroundColor==0){
            game.stage.backgroundColor = "#000000";
        }else{
            game.stage.backgroundColor = backgroundColor;
        }
        if(keyboard.space.isDown){
            changeColor();
        }
        //game.input.onDown.add(changeColor, this);
        if(keyboard.space.isDown){
           
            backgroundcolor=game.stage.backgroundColor;
        }
        
        //backgroundcolor=game.stage.backgroundColor;
        //var nameLabel = game.add.text(game.width/2, 80, 'Ns-Shaft',
        //{ font: '50px Arial', fill: '#ffffff' });
        //nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen
        
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'

        var EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
                distance=0;
                EnterKey.onDown.add(this.start, this);
                
            
            startLabel.events.onInputDown.add(this.start, this);
        },
        update:function  () {
            if(keyboard.up.isDown){
                //upKey.onDown.add(this.start, this);
                normal_c();
            }else if(keyboard.down.isDown){
                //upKey.onDown.add(this.start, this);
                hard_c();
            }
            if(keyboard.space.isDown){
                changeColor();
                
            }
            //game.input.onDown.add(changeColor, this);
            if(keyboard.space.isDown){
                backgroundcolor=game.stage.backgroundColor;
            }
            
        },
    start: function() {
    // Start the actual game
        distance=0;
        status = 'running';
        Username=document.getElementById("User").value||'unknown';
        this.clickSound = game.add.audio('click');
        this.clickSound.play();
        game.state.start('play');
    },
    normal_c: function() {
        // Start the actual game
        hard_mode=0;
        normal_text = game.add.text(game.width/2, game.height-230,
            'Normal', { fill: '#ff0000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        normal_text.anchor.setTo(0.5, 0.5);
        hard_text = game.add.text(game.width/2, game.height-180,
            'Hard', { fill: '#000000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        hard_text.anchor.setTo(0.5, 0.5);
        //normal_text.addColor("#ff0000", 0); //red
        //hard_text.addColor("#000000", 0);
    },
    hard_c: function() {
        hard_mode=1;
        normal_text = game.add.text(game.width/2, game.height-230,
            'Normal', { fill: '#000000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        normal_text.anchor.setTo(0.5, 0.5);
        hard_text = game.add.text(game.width/2, game.height-180,
            'Hard', { fill: '#ff0000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
        hard_text.anchor.setTo(0.5, 0.5);
        //hard_text.addColor("#ff0000", 0); //red
        //normal_text.addColor("#000000", 0);
    },
};
function normal_c() {
    // Start the actual game
    hard_mode=0;
    normal_text = game.add.text(game.width/2, game.height-230,
        'Normal', { fill: '#ff0000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
    normal_text.anchor.setTo(0.5, 0.5);
    hard_text = game.add.text(game.width/2, game.height-180,
        'Hard', { fill: '#000000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
    hard_text.anchor.setTo(0.5, 0.5);
    //normal_text.addColor("#ff0000", 0); //red
    //hard_text.addColor("#000000", 0);
}
function hard_c () {
    hard_mode=1;
    normal_text = game.add.text(game.width/2, game.height-230,
        'Normal', { fill: '#000000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
    normal_text.anchor.setTo(0.5, 0.5);
    hard_text = game.add.text(game.width/2, game.height-180,
        'Hard', { fill: '#ff0000',font: '35px Fontdiner Swanky',  fontWeight: 'bold', stroke: '#ffffff', strokeThickness: 9 });
    hard_text.anchor.setTo(0.5, 0.5);
    //hard_text.addColor("#ff0000", 0); //red
    //normal_text.addColor("#000000", 0);
}