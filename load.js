var loadState = {
    preload:function  () {

        game.load.spritesheet('conveyorRight', './assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', './assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', './assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', './assets/fake.png', 96, 36);
        game.load.image('wall', './assets/wall500.png');
        game.load.image('ceiling', './assets/ceiling.png');
        game.load.image('title', './assets/title.png');
        game.load.image('lboard', './assets/board.png');
        game.load.bitmapFont('myfont', 'assets/font/font.png', 'assets/font/font.fnt');
        
        //play
       game.load.spritesheet('player', './assets/player.png', 32, 32);
       game.load.image('normal', './assets/normal.png');
       game.load.image('sticky', './assets/sticky.png');
       game.load.image('nails', './assets/nails.png');
       game.load.spritesheet('bomb', './assets/bomb.png', 128, 128);
       game.load.spritesheet('explode', './assets/explode.png', 283.3, 237.5);
       game.load.spritesheet('conveyorRight', './assets/conveyor_right.png', 96, 16);
       game.load.spritesheet('conveyorLeft', './assets/conveyor_left.png', 96, 16);
       game.load.spritesheet('trampoline', './assets/trampoline.png', 96, 22);
       game.load.spritesheet('fake', './assets/fake.png', 96, 36);
       game.load.spritesheet('coin', './assets/coin.png', 64, 64);
       game.load.spritesheet('healthmeter', './assets/healthmeter.png', 100, 15);
       game.load.image('wall', './assets/wall500.png');
       game.load.image('wood', './assets/wood.png');
       game.load.image('start', './assets/start2.png');
       game.load.image('healthbar', './assets/healthbar.png');
       game.load.image('ceiling', './assets/ceiling.png');
       game.load.audio('hit', './assets/sounds/hit.wav');
       game.load.audio('jump', './assets/sounds/jump.ogg');
       game.load.audio('die', './assets/sounds/die.wav');
       game.load.audio('fake', './assets/sounds/fake.wav');
       game.load.audio('coin', './assets/sounds/coin.wav');
       game.load.audio('explode', './assets/sounds/explode.wav');
       game.load.audio('sticky', './assets/sounds/sticky.wav');
       game.load.audio('click', './assets/sounds/click.wav');
    },
    create: function() {
        document.getElementById("User").style.display= "none";
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.state.start('menu');
        
    },
    
};